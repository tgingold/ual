/**
 * @copyright Copyright (c) 2016 CERN
 * @author: Federico Vaga <federico.vaga@cern.ch>
 * @license: GPLv3
 */

#include <unistd.h>
#include <errno.h>
#include "ual-int.h"


/**
 * It writes a 32bit word at the given address
 *
 * @param[in] dev UAL device token returned by ual_open()
 * @param[in] addr offset within the selected BAR
 * @param[in] data 32bit word to write
 */
void ual_writel(struct ual_bar_tkn *dev, uint32_t addr, uint32_t data)
{
	struct ual_bar *bar = (struct ual_bar *)dev;

	if ((bar->flags & UAL_BAR_FLAGS_DEVICE_BE) !=
	    (bar->flags & UAL_BAR_FLAGS_HOST_BE)) {
		/* if original endianess and target one are not
		   compatible, do the swap byte order */
		data = ((data >> 24) & 0xff) |
		       ((data << 8) & 0xff0000) |
		       ((data >> 8) & 0xff00) |
		       ((data << 24) & 0xff000000);
	}
	*(volatile uint32_t *) (bar->ptr + addr) = data;
}


/**
 * It writes a 16bit word from the given address.
 *
 * @param[in] dev UAL device token returned by ual_open()
 * @param[in] addr offset within the selected BAR
 * @param[in] data 16bit word to write
 */
void ual_writew(struct ual_bar_tkn *dev, uint32_t addr, uint16_t data)
{
	struct ual_bar *bar = (struct ual_bar *)dev;

	if ((bar->flags & UAL_BAR_FLAGS_DEVICE_BE) !=
	    (bar->flags & UAL_BAR_FLAGS_HOST_BE)) {
		/* if original endianess and target one are not
		   compatible, do the swap byte order */
		data = ((data >> 8) & 0x00ff) |
		       ((data << 8) & 0xff00);
	}
	*(volatile uint16_t *) (bar->ptr + addr) = data;
}


/**
 * It reads a 32bit word from the given address.
 *
 * @param[in] dev UAL device token returned by ual_open()
 * @param[in] addr offset within the selected BAR
 * @return it returns a 32bit word
 */
uint32_t ual_readl(struct ual_bar_tkn *dev, uint32_t addr)
{
	struct ual_bar *bar = (struct ual_bar *)dev;
	uint32_t val = *(volatile uint32_t *) (bar->ptr + addr);

	if ((bar->flags & UAL_BAR_FLAGS_DEVICE_BE) !=
	    (bar->flags & UAL_BAR_FLAGS_HOST_BE)) {
		/* if original endianess and target one are not
		   compatible, do the swap byte order */
		val = ((val >> 24) & 0xff) |
		      ((val << 8) & 0xff0000) |
		      ((val >> 8) & 0xff00) |
		      ((val << 24) & 0xff000000);
	}

	return val;
}


/**
 * It reads a 16bit word from the given address.
 *
 * @param[in] dev UAL device token returned by ual_open()
 * @param[in] addr offset within the selected BAR
 * @return it returns a 16bit word
 */
uint16_t ual_readw(struct ual_bar_tkn *dev, uint32_t addr)
{
	struct ual_bar *bar = (struct ual_bar *)dev;
	uint16_t val = *(volatile uint16_t *) (bar->ptr + addr);

	if ((bar->flags & UAL_BAR_FLAGS_DEVICE_BE) !=
	    (bar->flags & UAL_BAR_FLAGS_HOST_BE)) {
		/* if original endianess and target one are not
		   compatible, do the swap byte order */
		val = ((val >> 8) & 0x00ff) |
		      ((val << 8) & 0xff00);
	}

	return val;
}
