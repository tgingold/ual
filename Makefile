DIRS = lib tools

.PHONY: all clean $(DIRS)

clean: TARGET = clean

all clean: $(DIRS)
$(DIRS):
	$(MAKE) -C $@ $(TARGET)
