#!/usr/bin/env python

from distutils.core import setup

setup(name='PyUAL',
      version='0.3',
      description='Python binding for UAL library',
      author='Federico Vaga',
      author_email='federico.vaga@cern.ch',
      maintainer="Federico Vaga",
      maintainer_email="federico.vaga@cern.ch",
      url='https://gitlab.cern.ch/cohtdrivers/ual',
      packages=['PyUAL'],
      license='LGPLv3',
     )

